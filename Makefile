all: wiringPi-osc

wiringPi-osc: wiringPi/libwiringPi.a wiringPi-osc.c
	gcc $(CFLAGS) $(LDFLAGS) -o $@ -std=gnu99 -g wiringPi-osc.c -I wiringPi -L wiringPi -lwiringPi `pkg-config --cflags --libs liblo`

wiringPi/libwiringPi.a:
	$(MAKE) -C wiringPi static

install: wiringPi-osc
	install -d $(DESTDIR)/usr/sbin
	install $< $(DESTDIR)/usr/sbin -m u+rwxs

clean:
	$(MAKE) -C wiringPi clean
	-$(RM) wiringPi-osc
