#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <lo/lo.h>

#include "wiringPi.h"

static int debug = 1;

static void
error_exit(char *msg)
{
  fprintf(stderr, "Error: %s\n", msg);
  exit(EXIT_FAILURE);
}

static int
modeHandler(const char *path, const char *types, lo_arg **argv,int argc, lo_message msg, void *user_data)
{
  char *p_port;
  char *p_end;
  char *p_mode;
  int port;
  int mode;

  assert(argc == 2);

  p_port = &argv[0]->s;
  p_mode = &argv[1]->s;

  port = strtol(p_port, &p_end, 0);
  if (*p_end != '\0') {
    port = -1;
  } else {
    if (!strcmp(p_mode, "output")) {
      mode = INPUT;
    } else if (!strcmp(p_mode, "input")) {
      mode = OUTPUT;
    } else if (!strcmp(p_mode, "pwm")) {
      mode = PWM_OUTPUT;
    } else if (!strcmp(p_mode, "clock")) {
      mode = GPIO_CLOCK;
    } else {
      port = -1;
    }
  }

  if (port >= 0) {
    pinMode(port, mode);
  }

  return 0;
}

static int
pullUpDnHandler(const char *path, const char *types, lo_arg **argv,int argc, lo_message msg, void *user_data)
{
  char *p_port;
  char *p_end;
  char *p_mode;
  int port;
  int mode;

  assert(argc == 2);

  p_port = &argv[0]->s;
  p_mode = &argv[1]->s;

  port = strtol(p_port, &p_end, 0);
  if (*p_end != '\0') {
    port = -1;
  } else {
    if (!strcmp(p_mode, "off")) {
      mode = PUD_OFF;
    } else if (!strcmp(p_mode, "down")) {
      mode = PUD_DOWN;
    } else if (!strcmp(p_mode, "up")) {
      mode = PUD_UP;
    } else {
      port = -1;
    }
  }

  if (port >= 0) {
    pullUpDnControl(port, mode);
  }

  return 0;
}

static int
dwriteHandler(const char *path, const char *types, lo_arg **argv,int argc, lo_message msg, void *user_data)
{
  char *p_port;
  char *p_end;
  char *p_value;
  int port;
  int value;

  assert(argc == 2);

  p_port = &argv[0]->s;
  p_value = &argv[1]->s;

  port = strtol(p_port, &p_end, 0);
  if (*p_end != '\0') {
    port = -1;
  } else {
    if (!strcmp(p_value, "high")) {
      value = 1;
    } else if (!strcmp(p_value, "low")) {
      value = 0;
    } else {
      port = -1;
    }
  }

  fprintf(stderr, "%s: port = %d, value = %d\n", __func__, port, value);

  if (port >= 0) {
    digitalWrite(port, value);
  }

  return 0;
}

static int
dreadHandler(const char *path, const char *types, lo_arg **argv,int argc, lo_message msg, void *user_data)
{
  char *p_port;
  char *p_end;
  char *p_value;
  int port;
  int value;
  static const char status[][5] = { "high", "low" };
  lo_address source;

  assert(argc == 1);

  p_port = &argv[0]->s;
  port = strtol(p_port, &p_end, 0);
  if (*p_end != '\0') {
    return 1;
  }

  value = digitalRead(port);
  assert(value == 0 || value == 1);
  fprintf(stderr, "%s: port = %d, value = %d\n", __func__, port, value);

  source = lo_message_get_source(msg);
  lo_send(source, "/rpi/wiringPi/digitalRead", "ss", p_port, status[value]);

  return 0;
}

/* catch any incoming messages and display them. returning 1 means that the
 * message has not been fully handled and the server should try other methods */
int generic_handler(const char *path, const char *types, lo_arg ** argv,
                    int argc, lo_message msg, void *user_data)
{
  int i;

  printf("generic_handler: path: <%s>", path);
  for (i = 0; i < argc; i++) {
    if (i != 0) {
      printf(", ");
    }
    printf("arg[%d]={'%c',", i, types[i]);
    lo_arg_pp((lo_type)types[i], argv[i]);
      printf("}, ");
  }
  printf("\n");
  fflush(stdout);

  return 1;
}


static void
oscErrorHandler(int num, const char *msg, const char *path)
{
  fprintf(stderr, "liblo server error %d in path %s: %s\n", num, path, msg);
}

int
main(void)
{
  lo_server_thread server;
  int result;

  result = wiringPiSetup();
  if (result < 0) {
    error_exit("Error in wiringPiSetup().");
  }

  if (setuid(getuid()) < 0) {
    error_exit("Error in setuid().");
  }

  server = lo_server_thread_new("7770", oscErrorHandler);

  /* add method that will match any path and args */
  if (debug) {
    lo_server_thread_add_method(server, NULL, NULL, generic_handler, NULL);
  }

  lo_server_thread_add_method(server, "/rpi/wiringPi/pinMode", "ss", modeHandler, NULL);
  lo_server_thread_add_method(server, "/rpi/wiringPi/pullUpDnControl", "ss", pullUpDnHandler, NULL);
  lo_server_thread_add_method(server, "/rpi/wiringPi/digitalWrite", "ss", dwriteHandler, NULL);
  lo_server_thread_add_method(server, "/rpi/wiringPi/digitalRead", "s", dreadHandler, NULL);

  lo_server_thread_start(server);

  while(1) {
    sleep(10);
  }
}

